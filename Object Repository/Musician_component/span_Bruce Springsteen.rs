<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Bruce Springsteen</name>
   <tag></tag>
   <elementGuidId>9b347278-6786-4688-b309-c39c039a4d7f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;reactContainer&quot;)/div[@class=&quot;app&quot;]/div[@class=&quot;mainWindow&quot;]/div[@class=&quot;results&quot;]/div[@class=&quot;musician&quot;]/a[@class=&quot;musician-link&quot;]/h3[@class=&quot;musician-link-title&quot;]/span[@class=&quot;musician-link-title-name&quot;][count(. | //span[@class = 'musician-link-title-name' and (text() = 'Bruce Springsteen' or . = 'Bruce Springsteen')]) = count(//span[@class = 'musician-link-title-name' and (text() = 'Bruce Springsteen' or . = 'Bruce Springsteen')])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='reactContainer']/div/div[2]/div[2]/div/a/h3/span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>musician-link-title-name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bruce Springsteen</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;reactContainer&quot;)/div[@class=&quot;app&quot;]/div[@class=&quot;mainWindow&quot;]/div[@class=&quot;results&quot;]/div[@class=&quot;musician&quot;]/a[@class=&quot;musician-link&quot;]/h3[@class=&quot;musician-link-title&quot;]/span[@class=&quot;musician-link-title-name&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='reactContainer']/div/div[2]/div[2]/div/a/h3/span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search!'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search musician:'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='remove'])[1]/preceding::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bruce Springsteen &amp; The E Street Band'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//span[2]</value>
   </webElementXpaths>
</WebElementEntity>
