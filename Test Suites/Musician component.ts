<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Musician component</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8e84fecd-69a5-4448-af5b-fe6635b733e4</testSuiteGuid>
   <testCaseLink>
      <guid>de5ed2c3-6a17-4f08-8015-c4fa17d500bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Musician/Results_Displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb38a07e-b92e-4d1b-aed9-1079d91555fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Musician/Log_out</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
