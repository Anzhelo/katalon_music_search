<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LogIn</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-18T22:15:37</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>401ce7e3-a913-4cfd-87f9-3104f6557e3c</testSuiteGuid>
   <testCaseLink>
      <guid>664b1992-b30b-4821-8a53-c825764fd7ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/Wrong_log_in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e51c5bb7-0ef0-46e6-bda3-b167480c1eea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/Correct_Log_In</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df1a8b2a-f315-4100-a736-79cd78839a57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/Correct_Log_In_Enter</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
