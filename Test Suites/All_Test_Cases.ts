<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>All_Test_Cases</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-19T00:26:32</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>afeaaa78-4129-46e0-a717-f750285878cc</testSuiteGuid>
   <testCaseLink>
      <guid>0477b35d-3ff9-4fab-a0c2-279ba75e487e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/Correct_Log_In</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68e964c9-1701-4a63-a7d9-3552ec17eec6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/Correct_Log_In_Enter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce3649c1-cca2-47ea-8925-0e02437064fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LogIn/Wrong_log_in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b24d38d8-181c-4bee-981a-aa7ae185f5a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Musician/Log_out</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7462c58c-670f-49e7-8187-6035daddfced</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Musician/Results_Displayed</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
